#include <stdlib.h>
#include <unistd.h>

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <chrono>
#include <iostream>
#include <thread>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#define CALC_FFMPEG_VERSION(a, b, c) (a << 16 | b << 8 | c)

static void SetEnv()
{
	std::cin.tie(nullptr);

	const std::string value =
	    "hwaccel;cuvid|hwaccel_device;0|video_codec;h264_cuvid|rtsp_transport;tcp";

	setenv("OPENCV_FFMPEG_CAPTURE_OPTIONS", value.c_str(), true);
	// setenv("OPENCV_FFMPEG_DEBUG", "1", true);
}

static int get_number_of_cpus(void)
{
#if LIBAVFORMAT_BUILD < CALC_FFMPEG_VERSION(52, 111, 0)
	return 1;
#elif defined _WIN32
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);

	return static_cast<int>(sysinfo.dwNumberOfProcessors);
#elif defined __linux__ || defined __HAIKU__
	return static_cast<int>(sysconf(_SC_NPROCESSORS_ONLN));
#elif defined __APPLE__
	int numCPU = 0;
	int mib[4];
	size_t len = sizeof(numCPU);

	// set the mib for hw.ncpu
	mib[0] = CTL_HW;
	mib[1] = HW_AVAILCPU; // alternatively, try HW_NCPU;

	// get the number of CPUs from the system
	sysctl(mib, 2, &numCPU, &len, NULL, 0);

	if (numCPU < 1)
	{
		mib[1] = HW_NCPU;
		sysctl(mib, 2, &numCPU, &len, NULL, 0);

		if (numCPU < 1)
			numCPU = 1;
	}

	return static_cast<int>(numCPU);
#else
	return 1;
#endif
}

static FILE *output_file = NULL;

struct CvFFmpegDecoder
{
	static CvFFmpegDecoder *create();
	static void destroy(CvFFmpegDecoder *dec);

	int init();
	int init_hw(AVCodecContext *ctx, const char *hwaccel_device);

	static AVPixelFormat get_hw_format(AVCodecContext *ctx, const AVPixelFormat *pix_fmts);
	static AVPixelFormat hw_pix_fmt;

	AVInputFormat *input_format;
	AVFormatContext *input_ctx;
	AVStream *video_stream;
	AVCodecContext *codec_ctx;
	AVCodec *codec;
	AVHWDeviceType hw_device_type;
	AVBufferRef *hw_device_ctx;
	AVPacket packet;
	AVDictionary *opts;
	int stream_index;
};

AVPixelFormat CvFFmpegDecoder::hw_pix_fmt;

CvFFmpegDecoder *CvFFmpegDecoder::create() { return new CvFFmpegDecoder; }
void CvFFmpegDecoder::destroy(CvFFmpegDecoder *dec)
{
	av_packet_unref(&dec->packet);

	if (dec->input_ctx)
		avformat_close_input(&dec->input_ctx);
	if (dec->codec_ctx)
		avcodec_free_context(&dec->codec_ctx);
	if (dec->hw_device_ctx)
		av_buffer_unref(&dec->hw_device_ctx);

	delete dec;
}

int CvFFmpegDecoder::init()
{
	av_log_set_level(AV_LOG_INFO);
	hw_device_type = AV_HWDEVICE_TYPE_CUDA;

	opts = NULL;
	hw_device_ctx = NULL;
	input_format = NULL;
	input_ctx = NULL;
	video_stream = NULL;
	codec_ctx = NULL;
	codec = NULL;
}

int CvFFmpegDecoder::init_hw(AVCodecContext *ctx, const char *hwaccel_device)
{
	int err = 0;

	if ((err = av_hwdevice_ctx_create(&hw_device_ctx, AV_HWDEVICE_TYPE_CUDA, hwaccel_device,
	                                  NULL, 0)) < 0)
	{
		av_log(ctx, AV_LOG_ERROR, "Failed to create specified HW device: %s\n",
		       hwaccel_device);
		return err;
	}

	ctx->hw_device_ctx = av_buffer_ref(hw_device_ctx);

	return err;
}

AVPixelFormat CvFFmpegDecoder::get_hw_format(AVCodecContext *ctx, const AVPixelFormat *pix_fmts)
{
	const AVPixelFormat *p;

	for (p = pix_fmts; *p != -1; p++)
	{
		if (*p == CvFFmpegDecoder::hw_pix_fmt)
			return *p;
	}

	av_log(ctx, AV_LOG_ERROR, "Failed to get HW surface format.\n");
	return AV_PIX_FMT_NONE;
}

namespace
{
inline CvFFmpegDecoder *cv_ffmpeg_decoder_create() { return CvFFmpegDecoder::create(); }
inline void cv_ffmpeg_decoder_destroy(CvFFmpegDecoder *decoder)
{
	CvFFmpegDecoder::destroy(decoder);
}
} // namespace

static void avframe_to_mat(unsigned char **data, int step, int width, int height, int cn)
{
	cv::Mat mat(height, width, CV_MAKETYPE(CV_8U, 3), data, step); //.copyTo(mat);
	std::cout << mat.size << std::endl;
}

struct CvOut
{
	CvOut() { cv::namedWindow("ffmpeg"); }
	~CvOut() { cv::destroyAllWindows(); }

	void show(cv::Mat &mat)
	{
		if (!mat.empty())
		{
			cv::imshow("ffmpeg", mat);
			// int k = cv::waitKey(5);
			// if (k == 113)
			// 	exit(0);
		}
	}
};

static int decode_write(AVCodecContext *avctx, AVPacket *packet)
{
	AVFrame *frame = NULL, *sw_frame = NULL;
	AVFrame *tmp_frame = NULL;
	uint8_t *buffer = NULL;
	int size;
	int ret = 0;

	ret = avcodec_send_packet(avctx, packet);
	if (ret < 0)
	{
		fprintf(stderr, "Error during decoding\n");
		return ret;
	}

	AVPixelFormat frame_format, sw_frame_format, tmp_frame_format;
	cv::Mat mat;

	while (1)
	{
		if (!(frame = av_frame_alloc()) || !(sw_frame = av_frame_alloc()))
		{
			fprintf(stderr, "Can not alloc frame\n");
			ret = AVERROR(ENOMEM);
			goto fail;
		}

		ret = avcodec_receive_frame(avctx, frame);
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
		{
			av_frame_free(&frame);
			av_frame_free(&sw_frame);
			return 0;
		}
		else if (ret < 0)
		{
			fprintf(stderr, "Error while decoding\n");
			goto fail;
		}

		if (frame->format == CvFFmpegDecoder::hw_pix_fmt)
		{
			/* retrieve data from GPU to CPU */
			sw_frame->format = AV_PIX_FMT_BGR24;
			if ((ret = av_hwframe_transfer_data(sw_frame, frame, 0)) < 0)
			{
				fprintf(stderr, "Error transferring the data to system memory\n");
				goto fail;
			}
			tmp_frame = sw_frame;
		}
		else
			tmp_frame = frame;

		frame_format = static_cast<AVPixelFormat>(frame->format);
		sw_frame_format = static_cast<AVPixelFormat>(sw_frame->format);
		tmp_frame_format = static_cast<AVPixelFormat>(tmp_frame->format);

		size = av_image_get_buffer_size(static_cast<AVPixelFormat>(tmp_frame->format),
		                                tmp_frame->width, tmp_frame->height, 1);
		buffer = static_cast<uint8_t *>(av_malloc(size));
		if (!buffer)
		{
			fprintf(stderr, "Can not alloc buffer\n");
			ret = AVERROR(ENOMEM);
			goto fail;
		}
		ret = av_image_copy_to_buffer(buffer, size, (const uint8_t *const *)tmp_frame->data,
		                              static_cast<const int *>(tmp_frame->linesize),
		                              static_cast<AVPixelFormat>(tmp_frame->format),
		                              tmp_frame->width, tmp_frame->height, 1);

		if (ret < 0)
		{
			fprintf(stderr, "Can not copy image to buffer\n");
			goto fail;
		}

		avframe_to_mat(&buffer, tmp_frame->linesize[0], tmp_frame->width, tmp_frame->height,
		               3); //, mat);

		// cv::imshow("ffmpeg", mat);

		if ((ret = fwrite(buffer, 1, size, output_file)) < 0)
		{
			fprintf(stderr, "Failed to dump raw data.\n");
			goto fail;
		}

	fail:
		av_frame_free(&frame);
		av_frame_free(&sw_frame);
		av_freep(&buffer);
		if (ret < 0)
			return ret;
	}
}

int main(int argc, char const *argv[])
{

	if (argc != 4)
	{
		fprintf(stderr, "Usage: %s <cuda device id> <stream url> <output file>\n", argv[0]);
		return 0;
	}

	const char *cuda_device_id = argv[1];
	const char *stream_url = argv[2];
	const char *out_file = argv[3];

	SetEnv();
	int err = 0;

	AVInputFormat *input_format = NULL;
	CvFFmpegDecoder *dec = cv_ffmpeg_decoder_create();

	dec->init();

	const char *options = getenv("OPENCV_FFMPEG_CAPTURE_OPTIONS");

	if (options == NULL)
		av_dict_set(&dec->opts, "rtsp_transport", "tcp", 0);
	else
		av_dict_parse_string(&dec->opts, options, ";", "|", 0);

	AVDictionaryEntry *entry = av_dict_get(dec->opts, "input_format", NULL, 0);
	if (entry != 0)
		input_format = av_find_input_format(entry->value);

	if (input_format)
		dec->input_format = input_format;

	err = avformat_open_input(&dec->input_ctx, stream_url, dec->input_format, &dec->opts);
	if (err != 0)
	{
		av_log(dec->input_ctx, AV_LOG_ERROR, "Cannot open input file '%s'\n", stream_url);
		return 1;
	}

	err = avformat_find_stream_info(dec->input_ctx, NULL);
	if (err < 0)
	{
		av_log(dec->input_ctx, AV_LOG_ERROR, "Cannot find input stream information.\n");
		return 1;
	}

	dec->stream_index =
	    av_find_best_stream(dec->input_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec->codec, 0);
	if (dec->stream_index < 0)
	{
		av_log(dec->input_ctx, AV_LOG_ERROR,
		       "Cannot find a video stream in the input file\n");
		return 1;
	}

	for (int i = 0;; i++)
	{
		const AVCodecHWConfig *config = avcodec_get_hw_config(dec->codec, i);
		if (!config)
		{
			av_log(dec->input_ctx, AV_LOG_ERROR,
			       "Decoder %s does not support device type %s.\n", dec->codec->name,
			       av_hwdevice_get_type_name(dec->hw_device_type));
			return 1;
		}
		if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX &&
		    config->device_type == dec->hw_device_type)
		{
			CvFFmpegDecoder::hw_pix_fmt = config->pix_fmt;
			break;
		}
	}

	if (!(dec->codec_ctx = avcodec_alloc_context3(dec->codec)))
		return AVERROR(ENOMEM);

	dec->codec_ctx->thread_count = get_number_of_cpus();

	dec->video_stream = dec->input_ctx->streams[dec->stream_index];
	if (avcodec_parameters_to_context(dec->codec_ctx, dec->video_stream->codecpar) < 0)
		return 1;

	dec->codec_ctx->get_format = dec->get_hw_format;

	if (dec->init_hw(dec->codec_ctx, cuda_device_id) < 0)
		return 1;

	err = avcodec_open2(dec->codec_ctx, dec->codec, NULL);
	if (err < 0)
	{
		av_log(dec->codec_ctx, AV_LOG_ERROR, "Failed to open codec for stream #%u\n",
		       dec->stream_index);
		return 1;
	}

	output_file = fopen(argv[3], "w+");

	/* actual decoding and dump the raw data */
	while (err >= 0)
	{
		if ((err = av_read_frame(dec->input_ctx, &dec->packet)) < 0)
			break;

		if (dec->stream_index == dec->packet.stream_index)
			err = decode_write(dec->codec_ctx, &dec->packet);

		av_packet_unref(&dec->packet);
	}

	/* flush the decoder */
	dec->packet.data = NULL;
	dec->packet.size = 0;
	err = decode_write(dec->codec_ctx, &dec->packet);

	if (dec)
		cv_ffmpeg_decoder_destroy(dec);

	if (output_file)
		fclose(output_file);

	std::cout << "exit success" << std::endl;

	return 0;
}
